#[allow(unused)]
use jlogger::{jdebug, jerror, jinfo, jwarn};
use std::fs::canonicalize;
use std::io::{Error, ErrorKind, Result};
use std::path::Path;

#[derive(Debug, Clone, Copy)]
pub enum PathEntryType {
    File,
    Dir,
    Any,
}

#[derive(Clone)]
pub struct PathEntry {
    path: String,
    cnt: u32,
    del: bool,
}

impl PathEntry {
    pub const CNT_TO_DESTROY: u32 = 300;
    pub fn new(path: &str, cnt: u32, etype: PathEntryType) -> Result<PathEntry> {
        let p = Path::new(path);

        if !p.exists() {
            return Err(Error::new(ErrorKind::NotFound, "Path does not found."));
        }

        if p.is_dir() {
            if let PathEntryType::File = etype {
                return Err(Error::new(ErrorKind::InvalidData, "Path is a directory."));
            }
        } else if let PathEntryType::Dir = etype {
            return Err(Error::new(
                ErrorKind::InvalidData,
                "Path is not a directory.",
            ));
        }

        Ok(PathEntry {
            path: format!("{}", canonicalize(p).unwrap().display()),
            cnt,
            del: false,
        })
    }

    pub fn get_cnt(&self) -> u32 {
        self.cnt
    }

    pub fn get_path(&self) -> String {
        self.path.clone()
    }

    pub fn get_alias(&self) -> String {
        self.path.clone()
    }

    pub fn reset_cnt(&mut self) {
        self.cnt = 0;
    }

    pub fn to_record(&self) -> Option<String> {
        if self.cnt >= PathEntry::CNT_TO_DESTROY {
            return None;
        }

        Some(format!("{},{}", self.get_path(), self.cnt))
    }

    pub fn should_remove(&self) -> bool {
        self.del
    }

    pub fn mark_remove(&mut self) {
        self.del = true;
    }
}
