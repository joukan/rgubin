use clap::{Arg, Command};
use jlogger::JloggerBuilder;
use log::LevelFilter;

mod path_db;
mod path_entry;

use crate::path_db::PathDB;
use crate::path_entry::PathEntryType;
use std::io::Result;

fn main() -> Result<()> {
    let matches = Command::new("rgubin")
        .version("0.2")
        .about("Utility to manage path/file history.")
        .arg(
            Arg::new("next-entry")
                .short('n')
                .long("next-entry")
                .help("Prinit nexted estimated entry.")
                .takes_value(true),
        )
        .arg(
            Arg::new("remove-entry")
                .short('r')
                .long("remove-entry")
                .help("Removed specified entry.")
                .takes_value(true),
        )
        .arg(
            Arg::new("print-entry")
                .short('p')
                .long("print-entry")
                .help("Print all entries recorded.")
                .takes_value(false),
        )
        .arg(
            Arg::new("record-file")
                .short('f')
                .long("record-file")
                .help("File to store records.")
                .takes_value(true),
        )
        .arg(
            Arg::new("record-type")
                .short('t')
                .long("record-type")
                .help("Record types.")
                .takes_value(true),
        )
        .arg(
            Arg::new("default-entry")
                .short('e')
                .long("default-entry")
                .help("Default entery.")
                .takes_value(true),
        )
        .arg(
            Arg::new("log-file")
                .short('l')
                .long("log-file")
                .help("File to store log messages.")
                .default_missing_value("/tmp/rgubin.log")
                .takes_value(true),
        )
        .get_matches();

    /*
     * we can not log to the console, if no logfile specified, just stop loging.
     */
    if let Some(logfile) = matches.value_of("log-file") {
        JloggerBuilder::new()
            .log_console(false)
            .log_file(Some(logfile), false)
            .max_level(LevelFilter::Debug)
            .build();
    } else {
        JloggerBuilder::new()
            .log_console(false)
            .max_level(LevelFilter::Off)
            .build();
    }

    let etype = match matches.value_of("record-type") {
        Some(s) if (s == "file" || s == "f") => PathEntryType::File,
        Some(s) if (s == "dir" || s == "d") => PathEntryType::Dir,
        _ => PathEntryType::Any,
    };

    let mut pdb = PathDB::new(
        matches.value_of("record-file"),
        etype,
        matches.value_of("default-entry"),
    )?;

    if matches.is_present("print-entry") {
        pdb.print_paths();
    } else if let Some(hint) = matches.value_of("next-entry") {
        pdb.print_next(hint);
    } else if let Some(hint) = matches.value_of("remove-entry") {
        pdb.remove_entry(hint);
    }

    Ok(())
}
