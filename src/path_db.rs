use crate::path_entry::{PathEntry, PathEntryType};
#[allow(unused)]
use jlogger::{jdebug, jerror, jinfo, jwarn};
use std::collections::HashMap;
use std::env;
use std::fs::{remove_file, OpenOptions};
use std::io::{prelude::*, BufReader, Result};
use std::path::Path;

pub struct PathDB {
    df: String,
    ht: HashMap<String, PathEntry>,
    defstr: String,
    etype: PathEntryType,
}

impl PathDB {
    pub fn new(df: Option<&str>, etype: PathEntryType, dstr: Option<&str>) -> Result<PathDB> {
        let mut data_file = env::var("RGUBIN_FILE").unwrap_or_else(|_| {
            format! {"{}/.rgubin",env::var("HOME").unwrap_or_else(|_| "/tmp".to_string())}
        });

        if let Some(f) = df {
            data_file = f.to_string();
        }

        let mut ht: HashMap<String, PathEntry> = HashMap::new();
        if let Ok(f) = OpenOptions::new().read(true).open(&data_file) {
            let reader = BufReader::new(&f);

            for line in reader.lines().flatten() {
                let v: Vec<&str> = line.split(',').collect();

                if v.len() < 2 {
                    continue;
                }

                let cnt = v[1].parse::<u32>().unwrap_or(u32::MAX);
                if cnt == u32::MAX {
                    continue;
                }

                match PathEntry::new(v[0], cnt + 1, etype) {
                    Ok(l) => {
                        ht.insert(String::from(v[0]), l);
                    }
                    Err(e) => jwarn!("Path {} is removed: {}", v[0], e),
                }
            }
        }

        let defstr;
        if let Some(ds) = dstr {
            defstr = ds.to_string();
        } else {
            defstr = env::current_dir()
                .unwrap()
                .into_os_string()
                .into_string()
                .unwrap();
        }

        Ok(PathDB {
            df: data_file,
            ht,
            defstr,
            etype,
        })
    }

    fn save_data_file(&self) {
        remove_file(&self.df).unwrap_or(());
        match OpenOptions::new().write(true).create(true).open(&self.df) {
            Ok(mut f) => {
                if !self.ht.is_empty() {
                    for (_, p) in self.ht.iter() {
                        if p.should_remove() {
                            continue;
                        }
                        if let Some(r) = p.to_record() {
                            writeln!(f, "{}", r).unwrap();
                        }
                    }

                    f.flush().unwrap();
                }
            }
            Err(_e) => {}
        }
    }

    fn get_vector(&self) -> Vec<PathEntry> {
        let mut v: Vec<PathEntry> = Vec::new();

        if !self.ht.is_empty() {
            for (_, p) in self.ht.iter() {
                v.push(p.clone());
            }
            //v.sort_by(|a, b| a.get_cnt().cmp(&b.get_cnt()));
            v.sort_by_key(|a| a.get_cnt())
        }

        v
    }

    pub fn print_paths(&self) {
        println!("{:>2}) {}", 0, self.defstr);
        let mut i = 1;

        if !self.ht.is_empty() {
            let v = self.get_vector();
            for t in &v {
                println!("{:>2}) {} ({})", i, t.get_alias(), t.get_cnt());
                i += 1;
            }
        }
    }

    fn print_by_exact_path(&mut self, hint: &str) -> bool {
        let mut ret = false;

        if let Ok(p) = PathEntry::new(hint, 0, self.etype) {
            println!("{}", p.get_path());
            self.ht.insert(p.get_path(), p);
            ret = true;
        }

        ret
    }

    fn print_by_number(&mut self, hint: &str) -> bool {
        let mut ret = false;

        if let Ok(i) = String::from(hint).parse::<u32>() {
            if i == 0 {
                let p = PathEntry::new(&self.defstr, 0, self.etype).unwrap();
                println!("{}", p.get_alias());
                self.ht.insert(p.get_path(), p);
                ret = true;
            } else {
                let v = self.get_vector();

                let j: usize = (i - 1) as usize;
                if j < v.len() {
                    println!("{}", v[j].get_path());
                    self.ht
                        .entry(v[j].get_path())
                        .and_modify(|entry| (*entry).reset_cnt());
                    ret = true;
                }
            }
        }

        ret
    }

    fn print_by_lastname(&mut self, hint: &str) -> bool {
        let mut ret = false;
        let v = self.get_vector();

        for t in &v {
            let p = t.get_path();
            let p = Path::new(&p);
            if p.ends_with(hint) {
                println!("{}", t.get_path());
                self.ht
                    .entry(t.get_path())
                    .and_modify(|entry| (*entry).reset_cnt());
                ret = true;
                break;
            }
        }

        ret
    }

    fn print_by_midname(&mut self, hint: &str) -> bool {
        let mut ret = false;
        let v = self.get_vector();

        for t in &v {
            let p = t.get_path();

            if let Some(i) = p.find(hint) {
                let np = format!("{}/{}", &p[..i], hint);

                ret = self.print_by_exact_path(&np);
                if ret {
                    break;
                }
            }
        }

        ret
    }

    fn print_by_partof_lastname(&mut self, hint: &str) -> bool {
        let mut ret = false;
        let v = self.get_vector();

        for t in &v {
            let p = t.get_path();
            if p.ends_with(hint) {
                println!("{}", t.get_path());
                self.ht
                    .entry(t.get_path())
                    .and_modify(|entry| (*entry).reset_cnt());
                ret = true;
                break;
            }
        }

        ret
    }

    pub fn print_next(&mut self, hint: &str) {
        if self.print_by_exact_path(hint) {
            self.save_data_file();
            return;
        }

        if self.print_by_number(hint) {
            self.save_data_file();
            return;
        }

        if self.print_by_lastname(hint) {
            self.save_data_file();
            return;
        }

        if self.print_by_midname(hint) {
            self.save_data_file();
            return;
        }

        if self.print_by_partof_lastname(hint) {
            self.save_data_file();
        }
    }

    fn remove_by_number(&mut self, hint: &str) -> bool {
        let mut ret = false;

        if let Ok(i) = String::from(hint).parse::<u32>() {
            let v = self.get_vector();
            let j: usize = (i - 1) as usize;
            if j < v.len() {
                println!("Remove {}", v[j].get_path());
                self.ht
                    .entry(v[j].get_path())
                    .and_modify(|entry| (*entry).mark_remove());
                ret = true;
            }
        }

        ret
    }

    pub fn remove_entry(&mut self, hint: &str) {
        if self.remove_by_number(hint) {
            self.save_data_file();
        }
    }
}
