#!/bin/bash
# Require following command:
# - rgubin
# - nvim
# - fzf


grdel (){
	sorted=$(sortpara -r -n $@)
	for tgt in $sorted
	do
		rgubin -r $tgt
	done
}

gru() {
	local topath=""

	if [ "x$@" != "x" ];then
		topath=$(rgubin -n $@)
	fi
	echo -----$topath
	if [ "x$topath" != "x" ];then
		cd $topath
	fi

	rgubin -p
	echo -----
	ls
}

_sel() {
	rgubin -p -t d
    exa -Dg
}

gr() {
	local topath=""

	if [ "x$@" != "x" ]; then
		topath=$(rgubin -t d -n $@)
    fi

	if [ "x$topath"  != "x" ];then 
		cd $topath
        return
    fi

	t=$( _sel | fzf --layout=reverse  --info=hidden -i --header="Selection:")
	index=$(echo $t | awk 'BETIN{FS=")"}{print $1}' | sed "s/)//g")
	gru $index
}

RGUBIN_CFG=$HOME/.gv.env
gw () {
    tf=""

	if [ "x$@" != "x" ]; then
		tf=$(rgubin -f $RGUBIN_CFG -n $@ -t f)

        if [ "x$tf" != "x" ]; then
            nvim $tf
            return
        fi

        tfr=$(rgubin -f $RGUBIN_CFG -p -e $@ -t f | fzf --layout=reverse  --info=hidden -i --header="Selection:")
    else
        tfr=$(rgubin -f $RGUBIN_CFG -p -t f -e "<Quit>" | fzf --layout=reverse  --info=hidden -i --header="Selection:")
    fi

	tf=$(echo $tfr | awk 'BETIN{FS=")"}{print $2}')
    if [ "x$tf" = "x" ]; then
        return
    fi

    if [ "x$tf" = "x<Quit>" ];then
        return
    fi

    nvim $tf
}
