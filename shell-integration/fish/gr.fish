function _gr
	set topath ""

	if test  "x$argv" != "x" 
		set topath (rgubin -n $argv -t d)
    end
	echo -----
	if test "x$topath" != "x" 
		cd $topath
    end

	rgubin -p -t d
	echo -----
	ls
end

function _sel
	rgubin -p -t d
    exa -Dg
end


function gr
	if test  "x$argv" != "x" 
		set topath (rgubin -t d -n $argv)
    end

	if test "x$topath" != "x" 
		cd $topath
        return
    end


	set t ( _sel | fzf --layout=reverse  --info=hidden -i --header="Selection:")
	set index (echo $t | awk 'BETIN{FS=")"}{print $1}' | sed "s/)//g")
	_gr $index

end
